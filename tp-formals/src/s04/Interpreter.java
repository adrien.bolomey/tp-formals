package s04;

import java.util.HashMap;
import java.util.logging.StreamHandler;

public class Interpreter {
  private static Lexer lexer;
  // TODO - A COMPLETER (ex. 3)

  private static HashMap<String, Integer> hashMap = new HashMap<>();

  public static int evaluate(String e) throws ExprException {
    lexer = new Lexer(e);
    int res = parseExpr();
    // test if nothing follows the expression...
    if (lexer.crtSymbol().length() > 0)
      throw new ExprException("bad suffix");
    return res;
  }

  private static int parseExpr() throws ExprException {
    // TODO - A COMPLETER
    if (lexer.crtSymbol().length() <= 0) throw new ExprException("Incomplete");
    int res = 0;
    res = parseTerm();
    while (lexer.isMinus() || lexer.isPlus()){
      if (lexer.isMinus()){
        lexer.goToNextSymbol();
        res -= parseTerm();
      } else {
        lexer.goToNextSymbol();
        res += parseTerm();
      }
    }
    return res;
  }

  private static int parseTerm() throws ExprException {
    // TODO - A COMPLETER
    if (lexer.crtSymbol().length() <= 0) throw new ExprException("Incomplete");
    int res = 0;
    res = parseFact();
    while (lexer.isStar() || lexer.isSlash()){
      if (lexer.isStar()){
        lexer.goToNextSymbol();
        res *= parseFact();
      } else {
        lexer.goToNextSymbol();
        res /= parseFact();
      }
    }
    return res;
  }

  private static int parseFact() throws ExprException {
    // TODO - A COMPLETER
    if (lexer.crtSymbol().length() <= 0) throw new ExprException("Incomplete");
    int res = 0;
    int arg = 0;
    if (lexer.isOpeningParenth()) {
      lexer.goToNextSymbol();
      res = parseExpr();
      if (!lexer.isClosingParenth()) throw new ExprException("Missing closing Parenth");
    } else if (lexer.isNumber()) res = lexer.intFromSymbol();
    else if (lexer.isIdent()) {
      String ident = lexer.crtSymbol();
      lexer.goToNextSymbol();
      if (lexer.isOpeningParenth()){
        lexer.goToNextSymbol();
        res = applyFct(ident, parseExpr());
      } else return applyFct(ident, arg);
    }

    boolean isClosing = lexer.isClosingParenth();
    lexer.goToNextSymbol();
    if (isClosing && lexer.isIdent()) {
      hashMap.put(lexer.crtSymbol(), res);
      lexer.goToNextSymbol();
    }
    return res;
  }


  private static int applyFct(String fctName, int arg) throws ExprException {
    int res = 0;
    switch (fctName) {
      case "abs":
        res = Math.abs(arg);
        break;
      case "sqr":
        res = (int) Math.pow(arg, 2);
        break;
      case "cube":
        res = (int) Math.pow(arg, 3);
        break;
      case "sqrt":
        if (arg < 0) throw new ExprException("Invalid square root");
        res = (int) Math.sqrt(arg);
        break;
      default:
        for (String key : hashMap.keySet()) {
          if (key.equals(fctName)) {
            return hashMap.get(key);
          }
        }
        throw new ExprException("Invalid function/ident");
    }
    return res;
  }
}
