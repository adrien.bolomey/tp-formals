package s05;

import java.util.Random;

/* The capacity can't be negative. It's 
   forbidden to remove when empty, or to add
   when full. The collection cannot be empty
   (resp. full) after adding (resp. removing) 
*/
public class BoundedIntStack {
  private int[] buf;     
  private int   top = -1;

  /*@ require: capacity >= 0
      ensure: isEmpty()
  @*/
  public BoundedIntStack(int capacity) {
    assert capacity >= 0;
    buf = new int[capacity];
    assert isEmpty();
  } 
  /*@ require: !isEmpty()
      ensure: \result == buf[\old(tap)] && !isFull()
  @*/
  public int pop() {
    assert !isEmpty();
    assert top-1 < buf.length;
    return buf[top--];
  }

  /*@ require: !isFull()
    ensure: buf[top] == x && !isEmpty()
  @*/
  public void push(int x) { 
    assert !isFull();
    buf[++top] = x;
    assert buf[top] == x;
    assert !isEmpty();
  }
  public boolean isEmpty() {
    return top == -1; 
  }
  public boolean isFull() {
    return top >= buf.length-1;
  }
  
  //------------------------------------------------------------------------
  
  public static boolean areAssertionsEnabled() {
    int ec=0; 
    assert (ec=1) == 1;
    return ec == 1; 
  }
  
  static Random rnd = new Random();
  
  static void playWithBoundedStack(BoundedIntStack s) {
    int nOperations = 1000;
    for(int i=0; i<nOperations; i++) {
      boolean doPush = rnd.nextBoolean();
      boolean doPop = !doPush;
      if(doPush && !s.isFull())
        s.push(rnd.nextInt());
      if(doPop && !s.isEmpty())
        s.pop();
    }
  }
  
  public static void main(String[] args) {
    if(!areAssertionsEnabled()) {
      System.out.println("Please enable assertions, with '-ea' VM option !!");
      System.exit(-1);
    }
    int nStacks = 1000;
    int maxSize = 50;
    for(int i=0; i<nStacks; i++) {
      BoundedIntStack s = new BoundedIntStack(rnd.nextInt(maxSize));
      playWithBoundedStack(s);
    }
    System.out.println("End of demo!");
  }

}