package s03;

import javax.script.ScriptEngine;

public class DiagSynt {
  //========================================================================
  static class ParseException extends Exception {
    public ParseException()           { super();    }
    public ParseException(String msg) { super(msg); }
  }
  //========================================================================
  private static String theWord;
  private static int    crtIndex;
  // ----------------------------------------------------------------------
  private static void initSymbolStream(String w) { 
    theWord = w; crtIndex=0; 
  }
  private static void goToNextSymbol() { 
    crtIndex++; 
  }
  private static boolean isEndOfSymbolStream() { 
    return crtIndex >= theWord.length();
  }
  private static char crtSymbol() { 
    if (isEndOfSymbolStream())
      return '\0';
    return theWord.charAt(crtIndex);   
  }
  // ----------------------------------------------------------------------
  private static void parse_S() throws ParseException {
    if (crtSymbol() != 'i') throw new ParseException();
    goToNextSymbol();
    parse_A();
    if (crtSymbol() != 'i') throw new ParseException();
    goToNextSymbol();
  }

  private static void parse_A() throws ParseException {
    if (crtSymbol() == '+') {
      goToNextSymbol();
      parse_B();
    } else parse_S();

  }

  private static void parse_B() throws ParseException {
    if (crtSymbol() != 'i') throw new ParseException();
    goToNextSymbol();
    parse_C();
    if (crtSymbol() != 'i') throw new ParseException();
    goToNextSymbol();

  }

  private static void parse_C() throws ParseException {
    if (crtSymbol() == '=') goToNextSymbol();
    else parse_B();
  }

  public static boolean isAccepted(String w) {
    initSymbolStream(w);
    try {
      parse_S();
    } catch (ParseException e) {
      return false;
    }
    return (isEndOfSymbolStream());
  }
  // ----------------------------------------------------------------------
  public static void main(String [] args) {
    if (args.length != 1) {
      System.out.println("Usage: java DiagSynt word");
      System.exit(-1);
    }
    System.out.println(isAccepted(args[0]));
  }
}
